package com.example.testapp2

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class PingController {

    @GetMapping("ping")
    fun ping(): ResponseEntity<Any> = ResponseEntity.ok().build()
}
