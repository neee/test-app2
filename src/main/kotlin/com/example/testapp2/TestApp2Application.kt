package com.example.testapp2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TestApp2Application

fun main(args: Array<String>) {
	runApplication<TestApp2Application>(*args)
}
